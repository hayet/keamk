<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<?php

// Affichages

// Utiliser echo pour afficher une chaine de caractères.
// Creer une nouvelle variable avec votre prenom à l'interieur
$prenom = 'Hayette';
echo '<p>Bonjour</p>' . $prenom . '!';
// Utiliser echo pour afficher une chaine du style, à l'aide de la variable :
// Bonjour,  (remplacer Thomas par votre prenom)
// Faire une condition qui affiche un texte different selon si c'est votre prenom ou pas.
if ($prenom == 'Hayette') {
    echo "<p> Bienvenue. </p>";
} else {
    echo "Au revoir";
}
// Utiliser une boucle while pour creer un affichage d'entiers de 1 à 10
$x = 0;
while ($x <= 10) {
    echo "The number is: $x <br>";
    $x++;
}
// Utiliser une boucle for pour faire la même chose
for ($x = 0; $x <= 10; $x++) {

}
// Creer une nouvelle fonction et y ajouter une des deux boucles. Faire appel à cette fonction
function boucle($x, $fin)
{
    while ($x <= $fin) {
        $x++;
        echo $x;
    }
}
// Faire en sorte que cette fonction compte de 1 à X. X étant un parametre variable de la fonction.
boucle(0, 15);
echo "<br/>";
// Faire des dessins

// Creer une boucle qui affiche une ligne de 12 caractères *
// Faire une autre boucle, contenant la premiere, 
// qui va permettre d'affiche un carré de caractères *
echo "<br/>";
for ($a = 1; $a <= 4; $a++) {
    for ($x = 0; $x <= 12; $x++) {
        echo "*";
    }
    echo "<br/>";
}
 



?>



</body>
</html>